# README #

### What is this repository for? ###

This repository contains several scripts we use for benchmarking HHsuite, HMMer and PSI-BLAST.

### How do I get set up? ###

If you want to contribute to HHsuite and test your changes:

* download the reference database: http://wwwuser.gwdg.de/~compbiol/data/hhsuite/benchmark/uniprot20_2015_06_bench.tgz
* run HHblits compareable to the calls in hhsuite_benchmark/rocx/hhblits/run_hhblits.sh through this reference database
* evaluate your generated score ffdatabases for different iterations compareable to the calls in hhsuite_benchmark/rocx/eval.sh
* compare your results to with the reference results handed out with the reference database

There is a alignment benchmark... we are still working to make it useable for others.