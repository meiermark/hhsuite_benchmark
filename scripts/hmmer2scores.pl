#!/usr/bin/perl
# Write score files for HMMER output

use strict;

$|=1; # autoflush on

if (scalar(@ARGV)<1) {
    print("
Write score files for HMMER output
Usage:   hmmer2scores.pl indir scop-DB Size-of-DB [options] 
Options:
\n"); 
    exit;
}

my $dir = $ARGV[0];
my @files;
if ($dir =~ /^(\S+)\/\S+?\.hmmer$/) {
    @files = ($dir);
    $dir = $1;
} else {
    @files = glob("$dir/*.hmmer"); 
}

my $scopdb = $ARGV[1];
my $NDB = 5287;#$ARGV[2];

my $file;
my $basename; # basename = name without extension
my $rootname; # rootname = basename without path
my $line;
my $n=0;
my $evalue_cutoff = 10000;
my %scop;

my $query;    # name of query sequence
my $qfam;     # family of query
my ($qsf,$qcf,$qcl); # query superfamily, fold, and class
my $qlen;     # length of query
my $template;   # name of template sequence
my $tfam;     # family of template
my $rel;      # relationship of query and template
my $rawscore; # raw score of hmmer
my $evalue;   # evalue of template with query
my $hit;      # index of hit in blast output file

# Read all SCOP headers
open (IN, $scopdb);
while ($line = <IN>) {
    if ($line =~ /^>(\S+) (\S+) /) {
	$scop{$1} = $2;
    }
}
close IN;

print scalar(keys(%scop)) . " SCOP headers found!\n";

print (scalar(@files)." files read in. Starting selection loop ...\n");

# For each query sequence do blast search and generate *.scores file from blast output
foreach $file (@files) {   
    $n++;
    if ($file =~/(.*)\..*?$/)   {$basename=$1;} else {$basename=$file;} # basename = name without extension
    if ($basename =~/.*\/(.*?)$/) {$rootname=$1;} else {$rootname=$basename;} # rootname = basename without path

    print(">>> $n: $file $basename $dir/$rootname <<<\n");

    my %found = ();
#    print "Keys in found: " . scalar(keys(%found)) . "\n";

    $query = $rootname;
    $query =~ s/_\d$//g;
    $qfam = $scop{$query};
    $qfam=~/^(\S\.\d+\.\d+)\.\d+$/;
    $qsf=$1;
    $qfam=~/^(\S\.\d+)\.\d+\.\d+$/;
    $qcf=$1;
    $qfam=~/^(\S)\.\d+\.\d+\.\d+$/;
    $qcl=$1;

    # Open blast output and read query name 
    open(INFILE,"$file") or die("Error: could not open $file!\n");

    open(OUTFILE,">$dir/$rootname.scores") or die("Error: could not open $dir/$rootname.scores: $!\n");
    print(OUTFILE "NAME  $query $qfam\n");
    print(OUTFILE "FAM   $qfam\n");
    print(OUTFILE "LENG  ???\n");
    print(OUTFILE "\n");
    print(OUTFILE "TARGET     FAMILY   REL LEN COL LOG2PVAL  SCORE   PROBAB MS\n");

    # Read hit list one by one and write into scores file
    $hit=0;
    while($line=<INFILE>) {
	if ($line=~/^\s*$/ || $line=~/^#/) { next; }
	if ($line=~/^\s*(\S+)\s+\-\s+\S+\s+\-\s+(\S+)\s+(\S+)\s+ /) {
	    $template=$1;
	    if (!exists $scop{$template}) { next; }
	    $tfam=$scop{$template};
	    $evalue=$2;
	    $rawscore=$3;
	    if ($evalue=~/^e/) {$evalue="1$evalue";}
	    if (!defined $tfam) {print("WARNING: wrong format in $file: line: $line"); next;}
	    $hit++;
	    
	    # Determine relationship $rel
	    if ($query eq $template) {
		$rel=5;
	    } elsif ($tfam eq $qfam) { 
		$rel=4;
	    } else {
		$tfam=~/^(\S\.\d+\.\d+)\.\d+$/;
		if ($1 eq $qsf) { 
		    $rel=3;
		} else {
		    $tfam=~/^(\S\.\d+)\.\d+\.\d+$/;
		    if ($1 eq $qcf) { 
			$rel=2;
		    } else {
			$tfam=~/^(\S)\.\d+\.\d+\.\d+$/;
			if ($1 eq $qcl) {$rel=1;} else {$rel=0;}
		    }
		}
	    }	
	    
	    printf(OUTFILE "%-10.10s %-10.10s %1i %3i %3i %7.2f %7.2f %6.2f %5.2f\n",$template,$tfam,$rel,0,0,-1.443*log($evalue/$NDB+1E-99),$rawscore,0.0,0);
	    $found{$template} = 1;
	}
    }

#    foreach $template (keys (%scop)) {
#	if (!exists $found{$template}) {
#	    $tfam = $scop{$template};
#	    if ($query eq $template) {
#		$rel=5;
#	    } elsif ($tfam eq $qfam) { 
#		$rel=4;
#	    } else {
#		$tfam=~/^(\S\.\d+\.\d+)\.\d+$/;
#		if ($1 eq $qsf) { 
#		    $rel=3;
#		} else {
#		    $tfam=~/^(\S\.\d+)\.\d+\.\d+$/;
#		    if ($1 eq $qcf) { 
#			$rel=2;
#		    } else {
#			$tfam=~/^(\S)\.\d+\.\d+\.\d+$/;
#			if ($1 eq $qcl) {$rel=1;} else {$rel=0;}
#		    }
#		}
#	    }	
#	    printf(OUTFILE "%-10.10s %-10.10s %1i %3i %3i %7.2f %7.2f %6.2f %5.2f\n",$template,$tfam,$rel,0,0,-1.443*log($evalue_cutoff/$NDB+1E-99),-10,0.0,0);
#	}
#    }

    close(OUTFILE);
    close(INFILE);
    print("\n");
}
exit(0);
