#!/usr/bin/env python

import hh_reader
import sys

def main():
    if(len(sys.argv) != 2):
        sys.stderr.write("Please specify one input file!")
        exit(1)
    
    for res in hh_reader.read_result(sys.argv[1]):
        print(res.query_id)
        break


if __name__ == "__main__":
    main()
