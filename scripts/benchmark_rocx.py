#!/usr/bin/env python
import os
import sys
import glob
from collections import namedtuple
from optparse import OptionParser
import ffindex
import progress_bar

HH_Score = namedtuple("HH_Score", ["query", "query_length", "hits"])
HH_Score_Entry = namedtuple("HH_Score_Hit",  "target family relation length aligned_columns log_pva score_aass probability score log_evalue")

#reads the SCOP annotation file for the benchmark
#each line contains tab-separated a query_id followed by its annotated SCOP families
def read_scop_annotation(annotation_file):
  fh = open(annotation_file)
  
  id_2_scops = dict()
  for line in fh:
    line = line.rstrip()
    tokens = line.split("\t")
    id_2_scops[tokens[0]] = tokens[1:]
    
  return id_2_scops


#returns the relation of the two SCOP families
#-1 excluded/invalid comparisons
#0 different class
#1 same class
#2 same fold
#3 same super_family
#4 same family
def get_relation(scop1, scop2):
  scop1_split = scop1.split(".")
  scop2_split = scop2.split(".")
      
  if (scop1_split[0] == "b" and int(scop1_split[1]) <= 70 and int(scop1_split[1]) >= 66 
      and scop2_split[0] == "b" and int(scop2_split[1]) <= 70 and int(scop2_split[1]) >= 66):
    return -1
  elif (scop1_split[0] == "c" and (int(scop1_split[1]) in [2, 3, 4, 5, 30, 66, 78, 79, 111]) 
        and scop2_split[0] == "c" and (int(scop2_split[1]) in [2, 3, 4, 5, 30, 66, 78, 79, 111])):
    return -1
  elif (scop1_split[0] == "i" or scop2_split[0] == "i"):
    return -1
  
  rel = 0
  for i in range(0, min(len(scop1), len(scop2))):
    if(scop1[i] != scop2[i]):
      return rel
      break
    if(scop1[i] == '.'):
      rel += 1

  return 4


#compares annotated SCOP FAMILIES of query and template
#returns the closest relation between the relations (see get_relation)
def get_max_relation(query, template, query2scop):
  if(query == template):
    return 5

  max_relation = -1
  for scop1 in query2scop[query]:
    for scop2 in query2scop[template]:
      max_relation = max(max_relation, get_relation(scop1, scop2))
      
#   print("query:    " + str(query2scop[query]))
#   print("template: " + str(query2scop[template]))
#   print("relation: " + str(max_relation))

  return max_relation
  

#returns the classification of the hit for query and template in this benchmark
def get_classification(query, template, query2scop, tp_relation, fp_relation, excl_relation):
  if(template not in query2scop.keys()):
    return "uk"
  
  max_relation = get_max_relation(query, template, query2scop)

  if(max_relation >= excl_relation or max_relation == -1):
    return "uk"
  elif(max_relation >= tp_relation):
    return "tp"
  elif(max_relation <= fp_relation):
    return "fp"
  else:
    return "uk"


def get_total_number_homologs(query, query2scop, tp_relation, fp_relation, excl_relation):
  nr_homologs = 0
  for id in query2scop.keys():
    classification = get_classification(query, id, query2scop, tp_relation, fp_relation, excl_relation)
    if(classification == "tp"):
      nr_homologs += 1
  return nr_homologs


def calculate_rocx_value(scores, query2scop, tp_relation, fp_relation, excl_relation, rocx):
    # query has no SCOP annotation
    if(scores.query not in query2scop):
        return (None, None)

    # total number of positives for given query
    number_of_homologs = get_total_number_homologs(scores.query, query2scop, tp_relation, fp_relation, excl_relation)
    
    # no homologs found
    if (number_of_homologs == 0):
      return (None, None)
    
    # calculate ROC data
    fp_count = 0
    tp_count = 0
    sum = 0
    
    seen_templates = set()
    for h in scores.hits:
        if(h.target in seen_templates):
            continue
        else:
            seen_templates.add(h.target)
            
        classification = get_classification(scores.query, h.target, query2scop, tp_relation, fp_relation, excl_relation)
        if (classification == "tp"):
            tp_count += 1
        elif (classification == "fp"):
            sum += tp_count
            fp_count += 1
            
        if (fp_count >= rocx):
            break

    seen_fps = fp_count

    # not enough false-positive hits
    while fp_count < rocx:
        sum += tp_count
        fp_count += 1

    rocx_value = sum / (1.0 * number_of_homologs * rocx)
    return (rocx_value, seen_fps)


def read_hh_scores_file(file_lines, ids):
  query = ""
  query_length = 0
  hits = []
  do_read = False
  
  for line in file_lines:
    line = line.rstrip()
    if(len(line) == 0):
      continue
    if line.startswith("TARGET"):
      do_read = True
    elif line.startswith("NAME"):
      query = line.split()[1]
    elif(line.startswith("LENG")):
      try:
        query_length = int(line.split()[1])
      except:
        query_length = -1
    elif do_read:
      tokens = line.split()


      if tokens[0] not in ids:
        continue
      elif(len(tokens) == 10):
        hit = HH_Score_Entry(tokens[0], tokens[1], int(tokens[2]), int(tokens[3]), int(tokens[4]), float(tokens[5]), float(tokens[6]), float(tokens[7]), float(tokens[8]), float(tokens[9]))
        hits.append(hit)
      elif(len(tokens) == 9):
        hit = HH_Score_Entry(tokens[0], tokens[1], int(tokens[2]), int(tokens[3]), int(tokens[4]), float(tokens[5]), 0.0, float(tokens[7]), float(tokens[6]), 0.0)
        hits.append(hit)
      else:
        sys.stderr.write("Line in unknown format: " + line + "\n")
        continue

  return HH_Score(query, query_length, hits)


def write_single_scores(query_2_rocx, output_prefix):
  fh = open(output_prefix+"_single_rocx.dat", "w")
  for query in query_2_rocx.keys():
    fh.write(query+"\t"+str(query_2_rocx[query])+"\n")
  fh.close()


def write_fp_statistic(query_2_fps, output_prefix):
  values = list(query_2_fps.values())
  fh = open(output_prefix+"_fp_stat.dat", "w")
  for i in range(max(values) + 1):
    fh.write(str(i)+"\t"+str(values.count(i))+"\n")
  fh.close()


def write_cum_sum_scores(query_2_rocx, output_prefix):
  all_rocx_values = []
  
  for query in query_2_rocx.keys():
    all_rocx_values.append(query_2_rocx[query])
    
  all_rocx_values.sort(reverse=False)
  cum_sum = []
  
  number_queries = len(all_rocx_values)
  number_left_queries = len(all_rocx_values)
  last_rocx_value = 0.0
  cum_sum.append((0.0, 1.0 * number_left_queries / number_queries))
  for rocx_value in all_rocx_values:
    if(last_rocx_value != rocx_value):
      cum_sum.append((rocx_value, 1.0 * number_left_queries / number_queries))
      
    last_rocx_value = rocx_value
    number_left_queries -= 1
    
  if(last_rocx_value != 1.0):
    cum_sum.append((1.0, 1.0 * number_left_queries / number_queries))
  
  fh = open(output_prefix+"_rocx.dat", "w")
  fh.write("#rocx\tprecentage\n")
  for point in cum_sum:
    fh.write(str(point[0])+"\t"+str(point[1])+"\n")
  fh.close()

  
def opt():
  parser = OptionParser()
  parser.add_option("--score_files", dest="input_glob",
    help="GLOB expression for all input score files", metavar="GLOB")
  parser.add_option("--score_ffindex", dest="input_ffindex",
    help="FFindex prefix for input score files", metavar="GLOB")
  parser.add_option("--annotation", dest="annotation_file",
    help="FILE containing all scop annotations for all ids in the benchmark set", metavar="FILE")
  parser.add_option("--excl_relation", dest="excl_relation",
    help="hits >= excl_relation are not regarded in the benchmark ", metavar="INT", type="int")
  parser.add_option("--tp_relation", dest="tp_relation",
    help="hits >= tp_relation are regarded as true positive hits in the benchmark ", metavar="INT", type="int")
  parser.add_option("--fp_relation", dest="fp_relation",
    help="hits <= fp_relation are regarded as false positive hits in the benchmark ", metavar="INT", type="int")
  parser.add_option("--rocx", dest="rocx",
    help="maximum number of false positive hits for the rocx statistics", metavar="INT", type="int")
  parser.add_option("-o", dest="output_prefix",
    help="prefix for output files", metavar="PREFIX")
  
  parser.set_defaults(excl_relation = 5)
  parser.set_defaults(tp_relation = 3)
  parser.set_defaults(fp_relation = 1)
  parser.set_defaults(rocx = 5)
  parser.set_defaults(output_suffix = "./")

  return parser


def check_input(options, parser):
  if(not options.input_glob and not options.input_ffindex):
    sys.stderr.write("ERROR: No input glob/ffindex specified!\n")
    parser.print_help()
    sys.exit(1)
    
  if(options.input_ffindex):
    if(not os.path.exists(options.input_ffindex+".ffindex")):
      sys.stderr.write("ERROR: Input ffindex index file does not exit! ("+options.input_ffindex+")")
      parser.print_help()
      sys.exit(1)
    if(not os.path.exists(options.input_ffindex+".ffdata")):
      sys.stderr.write("ERROR: Input ffdata data file does not exit! ("+options.input_ffindex+")")
      parser.print_help()
      sys.exit(1)
    
  if(not options.annotation_file or not os.path.exists(options.annotation_file)):
    sys.stderr.write("ERROR: annotation file not specified or not found!\n")
    parser.print_help()
    sys.exit(1)


def main():
  parser = opt()
  (options, args) = parser.parse_args()
  check_input(options, parser)

  id_2_scops = read_scop_annotation(options.annotation_file)
  query_2_rocx = dict()
  query_2_fps = dict()
  
  if(options.input_glob):
    for score_file in glob.glob(options.input_glob):
      fh = open(score_file)
      score_file_lines = fh.readlines()
      fh.close()
      
      scores = read_hh_scores_file(score_file_lines, id_2_scops.keys())
      (rocx_value, seen_fps) = calculate_rocx_value(scores, id_2_scops, options.tp_relation, options.fp_relation, options.excl_relation, options.rocx)
      
      if(rocx_value != None):
        query_2_rocx[scores.query] = rocx_value
        query_2_fps[scores.query] = seen_fps
        
  if(options.input_ffindex):
    input_data_file = options.input_ffindex+".ffdata"
    input_index_file = options.input_ffindex+".ffindex"
    input_data = ffindex.read_data(input_data_file)
    input_index = ffindex.read_index(input_index_file)
    
    for entry in progress_bar.progress_bar(input_index):
      count_results = 0
      
      score_file_lines = ffindex.read_lines(entry, input_data)
      
      scores = read_hh_scores_file(score_file_lines, id_2_scops.keys())
      (rocx_value, seen_fps) = calculate_rocx_value(scores, id_2_scops, options.tp_relation, options.fp_relation, options.excl_relation, options.rocx)
      if(rocx_value != None):
        query_2_rocx[scores.query] = rocx_value
        query_2_fps[scores.query] = seen_fps
    
  write_single_scores(query_2_rocx, options.output_prefix)
  write_cum_sum_scores(query_2_rocx, options.output_prefix)
  write_fp_statistic(query_2_fps, options.output_prefix)

if __name__ == "__main__":
  main()
