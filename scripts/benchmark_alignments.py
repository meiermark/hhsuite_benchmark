#!/usr/bin/env python

import os
import sys
from optparse import OptionParser

from Bio import pairwise2
from Bio.SubsMat import MatrixInfo as matlist

from Bio.Blast import NCBIStandalone

import hh_reader
import hmmer_reader
import tm_align_wrapper
import progress_bar
import ffindex

def read_alignment_pairs(alignment_pairs_file):
	alignment_pairs = set()

	fh = open(alignment_pairs_file, "r")
	for line in fh:
		if(line.startswith("#")):
			continue
		alignment_pairs.add(tuple(line.split()))
	fh.close()
	
	return alignment_pairs

def compare_alignments(aligned_query_reference, aligned_template_reference, aligned_query_comp, aligned_template_comp):
	map_query = map_sequences(get_sequence_from_ali(aligned_query_comp), get_sequence_from_ali(aligned_query_reference))
	map_template = map_sequences(get_sequence_from_ali(aligned_template_comp), get_sequence_from_ali(aligned_template_reference))

	aligned_reference = get_aligned(aligned_query_reference, aligned_template_reference)
	aligned_comp = get_aligned(aligned_query_comp, aligned_template_comp, map_query, map_template)

	tp = 0
	for ref in aligned_reference.items():
		for ali in aligned_comp.items():
			if(ali[0] == ref[0] and ali[1] == ref[1]):
				tp += 1

	return (tp, len(aligned_reference), len(aligned_comp))

def get_sequence_from_ali(ali):
	return ali.replace("-", "")

def map_sequences(from_sequence, to_sequence):
	matrix = matlist.blosum62
	gap_open = -10
	gap_extend = -0.5

	alns = pairwise2.align.globalds(from_sequence, to_sequence, matrix, gap_open, gap_extend)

	mapping = dict() 

	if(len(alns) == 0):
		sys.stderr.write("Warning: Could not map sequences (%s, %s)!\n" % (from_sequence, to_sequence))
		return mapping

	top_aln = alns[0]
	aln_from, aln_to, score, begin, end = top_aln
	
	#should be indices
	from_pos = 0
	to_pos = 0
	for i in range(len(aln_from)):
		if(aln_from[i] != "-"):
			from_pos += 1

		if(aln_to[i] != "-"):
			to_pos += 1

		if(aln_from[i] != "-" and aln_to[i] != "-"):
			mapping[from_pos] = to_pos

	return mapping

def get_aligned(query, template, map_query = None, map_template = None):
	aligned_pairs = dict()

	query_pos = 0
	template_pos = 0

	for i in range(len(query)):
		if(query[i] != "-"):
			query_pos += 1
		if(template[i] != "-"):
			template_pos += 1

		if(query[i] != "-" and template[i] != "-"):
			aligned_pairs[query_pos] = template_pos

	if(map_query != None and map_template != None):
		mapped_pairs = dict()
		for query_pos, template_pos in aligned_pairs.items():
			if(query_pos in map_query and template_pos in map_template):
				mapped_pairs[map_query[query_pos]] = map_template[template_pos]

		return mapped_pairs
	else:
		return aligned_pairs

def read_hhr_alignment(lines):
	queryAli = ""
	templateAli = ""

	for result in hh_reader.parse_result(lines):
		query_ali = result.query_ali
		template_ali = result.template_ali
		break

	return (queryAli, templateAli)

def read_psi_blast_alignment(lines):
	lowest_evalue = float("Inf")
	queryAli = ""
	templateAli = ""

	#TODO parse xml
# 	result_handle = open(aliFile, "r")
# 	blast_parser = NCBIStandalone.BlastParser()
# 	blast_iterator = NCBIStandalone.Iterator(result_handle, blast_parser)
# 
# 	for blast_record in blast_iterator:
# 		for alignment in blast_record.alignments:
# 			for hsp in alignment.hsps:
# 				if(hsp.expect < lowest_evalue):
# 					lowest_evalue = hsp.expect
# 					queryAli = hsp.query
# 					templateAli = hsp.template
# 
# 	return (queryAli, templateAli)

def read_hmmer_alignment(lines):
	result = hmmer_reader.parse(lines)
	
	lowest_domain_evalue = float("Inf")
	best_domain_ali = None

	for template in result.templates:
		for domain in template.domains:
			if(domain.domain_c_evalue < lowest_domain_evalue and domain.domain_alignment):
				best_domain_ali = domain.domain_alignment
				lowest_domain_evalue = domain.domain_c_evalue

	if(best_domain_ali):
		return(best_domain_ali.query_ali, best_domain_ali.template_ali)
	else:
		return("", "")


def benchmark_all_alignments(alignment_pairs, input_ffindex, suffix, reader, tm_dir, output_file):
	fh = open(output_file, "w")
	fh.write("#query\ttemplate\tsensitivity\tprecision\n")
	
	input_data_file = input_ffindex+".ffdata"
	input_index_file = input_ffindex+".ffindex"
	input_data = ffindex.read_data(input_data_file)
	input_index = ffindex.read_index(input_index_file)

	for alignment_pair in progress_bar.progress_bar(alignment_pairs, "evaluate alignments: ", 100):
		queryAli = ""
		templateAli = ""
		queryRef = ""
		templateRef = ""

		ref_ali_file = os.path.join(tm_dir, alignment_pair[0]+"-"+alignment_pair[1]+".tmalign")
		if(not os.path.exists(ref_ali_file)):
			sys.stderr.write("Warning: TMalign file %s with the reference alignment does not exist!\n" % (ref_ali_file))
			continue

		ref_ali = tm_align_wrapper.read_tm_alignment(ref_ali_file)
		query_ref = ref_ali.query_ali
		template_ref = ref_ali.template_ali
		
		entry_name = alignment_pair[0]+"-"+alignment_pair[1]+suffix
		entry = ffindex.get_entry_by_name(entry_name, input_index)
		
		if(entry == None):
			sys.stderr.write("Warning: Alignment not found in input ffindex\n")
			fh.write("%s\t%s\t%s\t%s\n" % (alignmentPair[0], alignmentPair[1], 0, 0))
			continue
			
		entry_lines = ffindex.read_lines(entry, input_data)
		(query_ali, template_ali) = reader(entry_lines)

		(tp, tp_ref, tp_comp) = compare_alignments(query_ref, template_ref, query_ali, template_ali)

		if(tp_ref == 0 or tp_comp == 0):
			sys.stderr.write("Warning: Invalid alignments (length of tm ali: %s) (length of ali: %s)!\n" % (tp_ref, tp_comp))
			fh.write("%s\t%s\t%s\t%s\n" % (alignment_pair[0], alignment_pair[1], 0, 0))
			continue

		sensitivity = 1.0 * tp / tp_ref
		precision = 1.0 * tp / tp_comp
		
		fh.write("%s\t%s\t%s\t%s\n" % (alignment_pair[0], alignment_pair[1], str(sensitivity), str(precision)))
	fh.close()

def opt():
	parser = OptionParser()
	parser.add_option("--input_ffindex", dest="input_ffindex",
		help="DIR with alignments in subdirectories for the chosen iteration comparisions", metavar="DIR")
	parser.add_option("--tm_dir", dest="tm_dir",
		help="DIR with reference tm alignments", metavar="DIR")
	parser.add_option("--alignment_pairs", dest="alignment_pairs_file",
		help="FILE with alignment pairs, their relations and tmScores", metavar="FILE")
	parser.add_option("--format", dest="format",
		help="format of the input files", metavar="[hmmer|hhr|blast]")
	parser.add_option("-o", dest="output_file",
		help="FILE for output of the statistics", metavar="FILE")

	return parser

def checkInput(options, parser):
	if(not options.input_ffindex):
		sys.stderr.write("ERROR: input ffindex not specified or not found!\n")
		parser.print_help()
		sys.exit(1)
	elif(not os.path.exists(options.input_ffindex+".ffindex") or not os.path.exists(options.input_ffindex+".ffdata")):
		sys.stderr.write("ERROR: input ffindex ("+options.input_ffindex+".ff{index,data}"+") not found!\n")
		parser.print_help()
		sys.exit(1)

	if(not options.tm_dir or not os.path.exists(options.tm_dir)):
		sys.stderr.write("ERROR: directory with tm alignments not specified or not found!\n")
		parser.print_help()
		sys.exit(1)

	if(not options.alignment_pairs_file or not os.path.exists(options.alignment_pairs_file)):
		sys.stderr.write("ERROR: file with alignment pairs not specified or not found!\n")
		parser.print_help()
		sys.exit(1)

	if(not options.output_file):
		sys.stderr.write("ERROR: output file not specified!\n")
		parser.print_help()
		sys.exit(1)

	if(not options.format in ("hmmer", "hhr", "blast")):
		sys.stderr.write("ERROR: unknown input format!")
		parser.print_help()
		sys.exit(1)


def main():
	parser = opt()
	(options, args) = parser.parse_args()
	checkInput(options, parser)

	alignment_pairs = read_alignment_pairs(options.alignment_pairs_file)

	suffix = ""
	reader = None
	if(options.format == "hmmer"):
		suffix = ".hmmer_results"
		reader = read_hmmer_alignment
	elif(options.format == "hhr"):
		suffix = ".hhr"
		reader = read_hhr_alignment
	elif(options.format == "blast"):
		suffix = ".xml"
		reader = read_psi_blast_alignment

	benchmark_all_alignments(alignment_pairs, options.input_ffindex, suffix, reader, options.tm_dir, options.output_file)

if __name__ == "__main__":
	main()
