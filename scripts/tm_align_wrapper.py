#!/usr/bin/env python

import subprocess
from collections import namedtuple

TM_ALIGN_BINARY="/home/meiermark/opt/TMalign"

TMalignResult = namedtuple("TMalignResult", ["tm_score", "aligned_length", "rmsd", "identity", "query_ali", "template_ali", "raw_output"])


def run_tm_align(queryPDBFilePath, templatePDBFilePath, outputFile=None):
	result = subprocess.check_output([TM_ALIGN_BINARY, queryPDBFilePath, templatePDBFilePath], stderr=subprocess.STDOUT)
	return process_tm_result(result)


def process_tm_result(tm_align_output):
#	TMalignOutput = TMalignOutput.decode("utf-8")
	lines = tm_align_output.splitlines()
	tm_score = 0.0
	identity = 0.0
	rmsd = 0.0
	aligned_length = 0
	query_ali = ""
	template_ali = ""
	for i in range(len(lines)):
		lines[i] = lines[i]
		if(lines[i].startswith('(":" denotes aligned residue pairs')):
			query_ali = lines[i+1]
			template_ali = lines[i+3]
			break
		#Aligned length=  164, RMSD=   3.30, Seq_ID=n_identical/n_aligned= 0.146
		elif(lines[i].startswith("Aligned length=")):
			tokens = lines[i].split(",")
			aligned_length = tokens[0].split("=")[1]
			rmsd = tokens[1].split("=")[1]
			identity = tokens[2].split("=")[2]
		elif(lines[i].startswith("TM-score") and lines[i].endswith("Chain_1)")):
			tm_score = float(lines[i].split()[1])

	return(TMalignResult(tm_score, int(aligned_length), float(rmsd), float(identity), query_ali, template_ali, tm_align_output))


def read_tm_alignment(alignmentFile):
	with open(alignmentFile, "r") as fh:
		result = fh.read()
		return process_tm_result(result)
