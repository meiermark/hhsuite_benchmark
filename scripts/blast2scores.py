#!/usr/bin/env python

import sys
import re
import os
from glob import glob
from math import log

def get_relation(query_name, query_family, template_name, template_family):
    if query_name == template_name:
        return 5
    elif query_family == template_family:
        return 4
    else:
        query_hierarchy = query_family.split(".")
        template_hierarchy = template_family.split(".")

        if query_family[0] != template_family[0]:
            return 0
        elif query_family[1] != template_family[1]:
            return 1
        elif query_family[2] != template_family[2]:
            return 2
        else:
            return 3
            

NDB=5287
input_file = sys.argv[1]

pattern = re.compile("\s*(d\S+)\s+(\S+)\s.+\s(\d*\.?\d+)\s+([-+]?[0-9]*\.?[0-9]*[eE]?[-+]?[0-9]+)\s*$")

for input_file in glob(sys.argv[1]):
    template2hit = dict()
    query_name = ""
    query_length = ""
    query_family = ""

    with open(input_file, "r") as fh:
        for line in fh:
            if line.startswith("Query="):
                tokens = line.split()
                query_name = tokens[1]
                query_family = tokens[2]
            elif line.startswith("Length="):
                query_length = int(line.strip().split("=")[1])
            elif line.strip().startswith("d"):

                result = pattern.match(line)
                if not result:
                    continue
        
                template = result.group(1)
                family = result.group(2)
                score = result.group(3)
                evalue = result.group(4)

                if evalue[0] == "e":
                    evalue = "1" + evalue

                if template in template2hit and template2hit[template][3] < float(evalue):
                    continue
                else:
                    template2hit[template] = (template, family, float(score), float(evalue))

    output_basename = os.path.basename(input_file).split(".")
    del output_basename[-1]
    output_file = os.path.join(os.path.dirname(input_file), ".".join(output_basename)+".scores")

    with open(output_file, "w") as fh:
        fh.write("NAME  "+query_name+"\n")
        fh.write("FAM   "+query_family+"\n")
        fh.write("LENG  "+str(query_length)+"\n")
        fh.write("\n")
        fh.write("TARGET                FAMILY   REL  LEN  COL  LOG-PVA  S-AASS PROBAB  SCORE  LOG-EVAL\n")

        values = sorted(template2hit.values(), key=lambda x: x[3])
        for value in values:
            relation = get_relation(query_name, query_family, value[0], value[1])
            fh.write("{0:10s} {1:10s} {2:1} {3:3} {4:3} {5:7.2} {6:7.2} {7:7.2} {8:7.2} {9:7.2}\n".format
                    (value[0], value[1], relation, 100, 100, 0.0, value[2], 100.0, value[2], -1.443 * log(value[3] / NDB + 1E-99)))


