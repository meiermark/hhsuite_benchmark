#!/usr/bin/env python

import ffindex
import sys

input_ffindex = sys.argv[1]
input_mapping = sys.argv[2]
output_ffindex = sys.argv[3]

mapping = {}
seen_names = set()
with open(input_mapping, "r") as fh:
    for line in fh:
        former_name, new_name = line.split()
        if new_name not in seen_names:
            mapping[former_name] = new_name
            seen_names.add(new_name)

old_entries = ffindex.read_index(input_ffindex)

new_entries = []
for entry in old_entries:
    if entry.name in mapping:
        new_entry = ffindex.FFindexEntry(mapping[entry.name], entry.offset, entry.length)
        new_entries.append(new_entry)

ffindex.write_entries_to_db(new_entries, output_ffindex)
