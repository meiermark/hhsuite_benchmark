#!/usr/bin/env python

from collections import namedtuple
import sys

HmmerResult = namedtuple("HmmerResult", ["query", "templates"])
SeqInfo = namedtuple("SeqInfo", ["evalue", "score", "bias", "best_domain_evalue", "best_domain_score", "best_domain_bias", "expected_domains", "number_domains", "template_id", "domains"])
DomInfo = namedtuple("DomInfo", ["domain_number", "domain_score", "domain_bias", "domain_c_evalue", "domain_i_evalue", "domain_template_from", "domain_template_to", "domain_query_from", "domain_query_to", "domain_alignment"])
DomAli = namedtuple("DomAli", ["index", "query_ali", "template_ali"])


def parse(lines):
	new_lines = []
	for line in lines:
		if len(line) > 0:
			new_lines.append(line)
	lines = new_lines


	query = ""
	
	seqTable = list()
	isSeqTable = False

	isDomainAnnotation = False
	isDomainTable = False
	isDomainAlignment = False
	domInfos = dict()

	domAlis = None
	domTable = None
	template_id = None

	i = 0
	while i < len(lines):
		if(lines[i].startswith("Query:")):
			query = lines[i].split()[1]
		elif(lines[i].startswith("Scores for complete sequences (score includes all domains):")):
			i += 3
			isSeqTable = True
		elif(lines[i].startswith("Domain annotation for each sequence (and alignments):")):
			isSeqTable = False
			isDomainAnnotation = True
		elif(lines[i].startswith("Internal pipeline statistics summary:")):
			break
		elif(isSeqTable):
			#7.1e-08   18.7   0.0    1.1e-07   18.0   0.0    1.3  1  d1k8qa_   c.69.1.6 (A:) Gastric lipase {Dog (Canis familiaris) [TaxId: 9615]}
			items = lines[i].split()

			if(len(items) < 9):
				i += 1
				continue

			evalue = float(items[0])
			score = float(items[1])
			bias = float(items[2])

			best_domain_evalue = float(items[3])
			best_domain_score = float(items[4])
			best_domain_bias = float(items[5])

			expected_domains = float(items[6])
			number_domains = int(items[7])

			template_id = items[8]

			seqInfo = SeqInfo(evalue, score, bias, best_domain_evalue, best_domain_score, best_domain_bias, expected_domains, number_domains, template_id, None)
			seqTable.append(seqInfo)
		elif(isDomainAnnotation):
			#>> d1k8qa_  c.69.1.6 (A:) Gastric lipase {Dog (Canis familiaris) [TaxId: 9615]}
			if(lines[i].startswith(">>")):
				template_id = lines[i].split()[1]
				isDomainTable = True
				isDomainAlignment = False

				domAlis = dict()
				domTable = dict()

				domInfos[template_id] = (domTable, domAlis)

				i += 2
			elif(lines[i].lstrip().startswith("Alignments for each domain:")):
				isDomainTable = False
				isDomainAlignment = True
			elif(isDomainTable):
				# #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
				# 1 !   18.0   0.0   1.1e-07   1.1e-07      13      91 ..      63     157 ..      53     185 .. 0.63
				items = lines[i].split()

				if(len(items) < 16):
					i += 1
					continue

				domain_number = int(items[0])
				domain_score = float(items[2])
				domain_bias = float(items[3])
				domain_c_evalue = float(items[4])
				domain_i_evalue = float(items[5])
				domain_template_from = int(items[6])
				domain_template_to = int(items[7])
				domain_target_from = int(items[9])
				domain_target_to = int(items[10])
				
				domInfo = DomInfo(domain_number, domain_score, domain_bias, domain_c_evalue, domain_i_evalue, domain_template_from, domain_template_to, domain_target_from, domain_target_to, None)
				domTable[domain_number] = domInfo

			elif(isDomainAlignment):
				#  == domain 1  score: 18.0 bits;  conditional E-value: 1.1e-07
				#                 xxxxxxxxxxxxxxxxxx......xxxxxxxxxxxxxxxxxxxxx........xxxxxxxxx........xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx RF
				#  d1cvla_-i1  13 LvHGllgfdkllglvdyW......ygikeaLeanGatVfvanvsg........fgssegpev........rgeqLaaqveevlaatgaskVnliGHSqGGl 91 
				#                 L HGll +        +W      + ++  L+++G+ V+ +n +g        + s +++e            +L a ++ +l++tg++k++ +GHSqG  
				#     d1k8qa_  63 LQHGLLAS------ATNWisnlpnNSLAFILADAGYDVWLGNSRGntwarrnlYYSPDSVEFwafsfdemAKYDLPATIDFILKKTGQDKLHYVGHSQGTT 157
				#                 45888654......4556332233344556888888888776655332222222222222212222222145788899*********************65 PP
				if(lines[i].lstrip().startswith("==")):
					items = lines[i].split()
					domain_number = int(items[2])
					score = float(items[4])
					evalue = float(items[8])

					query_id = ""

					query_from = 1E12
					query_to = -1
					query_ali = ""

					template_from = 1E12
					template_to = -1
					template_ali = ""

					if(lines[i+1].rstrip().endswith("RF") or lines[i+1].rstrip().endswith("CS")):
						i += 2 
					else:
						i += 1

					while True:
			#			try:
							items = lines[i].split()
							query_id = items[0]
							if(items[1] != "-"):
								query_from = min(int(items[1]), query_from)
							if(items[3] != "-"):
								query_to = max(int(items[3]), query_to)
							query_ali += items[2]

							i += 2
							items = lines[i].split()
							if(items[1] != "-"):
								template_from = min(int(items[1]), template_from)
							if(items[3] != "-"):
								template_to = max(int(items[3]), template_to)
							template_ali += items[2]
					
							i += 1
							while(lines[i].endswith("PP\n") or lines[i].endswith("RF\n") or lines[i] == "\n" or lines[i].endswith("CS\n")):
								i += 1
							
							items = lines[i].split()
							if(len(items) != 4 or items[0] != query_id):
								i -= 1
								break
			#			except:
			#				print(lines[i])
					
					domAli = DomAli(domain_number, query_ali.upper().replace(".", "-"), template_ali.upper())
					domAlis[domain_number] = domAli
				
		i += 1

	#shrink output
	entries = list()
	for seqInfo in seqTable:
		doms = list()
		if(seqInfo.template_id in domInfos):
			(domInfos, domAlis) = domInfos[seqInfo.template_id]
			for domIndex in domInfos:
				domInfo = domInfos[domIndex]
			
				dom_ali = None
				if(domIndex in domAlis):
					dom_ali = domAlis[domIndex]
				
				domInfo = domInfo._replace(domain_alignment=dom_ali)
			
				doms.append(domInfo)
			
		seqInfo = seqInfo._replace(domains=doms)
		entries.append(seqInfo)

	hmmerResult = HmmerResult(query, entries)
	return(hmmerResult)


def read(fileName):
	with open(fileName, "r") as fh:
		lines = fh.readlines()
		return parse(lines)

if __name__ == "__main__":
	res = read(sys.argv[1])
	
	print("Query: %s" % res.query)
	for seqInfo in res.templates:
		print("Template: %s" % seqInfo.template)
		for domain in seqInfo.domains:
			print("Domain: %s" % (domain.domain_number))
			if domain.domain_alignment != None:
				print(domain.domain_alignment.query_ali)
				print(domain.domain_alignment.template_ali)
				
				
