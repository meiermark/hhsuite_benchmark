#!/usr/bin/env python

import os
import sys
from optparse import OptionParser
import tempfile

import progress_bar
import benchmark_alignments
import ffindex
import hh_reader
import tm_align_wrapper

def benchmark_all_alignments(alignment_pairs, query_ffindex, template_ffindex, hhalign_binary, hhalign_parameters, tm_dir, output_file):
    fh = open(output_file, "w")
    fh.write("#query\ttemplate\tsensitivity\tprecision\n")

    query_data = ffindex.read_data(query_ffindex + ".ffdata")
    query_index = ffindex.read_index(query_ffindex + ".ffindex")

    template_data = ffindex.read_data(template_ffindex + ".ffdata")
    template_index = ffindex.read_index(template_ffindex + ".ffindex")

    query_file = tempfile.mkstemp()[1]
    template_file = tempfile.mkstemp()[1]
    ali_file = tempfile.mkstemp()[1]


    for alignment_pair in progress_bar.progress_bar(alignment_pairs, "evaluate alignments: ", 100):
        queryAli = ""
        templateAli = ""
        queryRef = ""
        templateRef = ""

        ref_ali_file = os.path.join(tm_dir, alignment_pair[0]+"-"+alignment_pair[1]+".tmalign")
        if(not os.path.exists(ref_ali_file)):
            sys.stderr.write("Warning: TMalign file %s with the reference alignment does not exist!\n" % (ref_ali_file))
            #      fh.write("%s\t%s\t%s\t%s\n" % (alignment_pair[0], alignment_pair[1], "NA", "NA"))
            continue

        ref_ali = tm_align_wrapper.read_tm_alignment(ref_ali_file)
        query_ref = ref_ali.query_ali
        template_ref = ref_ali.template_ali

        query_entry = ffindex.get_entry_by_name(alignment_pair[0]+".a3m", query_index)
        if(query_entry == None):
            sys.stderr.write("Warning: could not find %s in %s!" % (alignment_pair[0]+".a3m", query_ffindex))
            #      fh.write("%s\t%s\t%s\t%s\n" % (alignment_pair[0], alignment_pair[1], "NA", "NA"))
            continue

        ffindex.write_entry_to_file(query_entry, query_data, query_file)

        template_entry = ffindex.get_entry_by_name(alignment_pair[1]+".a3m", template_index)
        if(template_entry == None):
            sys.stderr.write("Warning: could not find %s in %s!" % (alignment_pair[1]+".a3m", template_ffindex))
            #      fh.write("%s\t%s\t%s\t%s\n" % (alignment_pair[0], alignment_pair[1], "NA", "NA"))
            continue

        ffindex.write_entry_to_file(template_entry, template_data, template_file)

        #run hhalign
        os.system(" ".join([hhalign_binary, "-i", query_file, "-t", template_file, "-o", ali_file, "-v 0", hhalign_parameters]))

        if(not os.path.exists(ali_file)):
            sys.stderr.write("Warning: Alignment file not found (%s)!\n" % (ali_file))
            fh.write("%s\t%s\t%s\t%s\n" % (alignment_pair[0], alignment_pair[1], "NA", "NA"))
            continue

        results = hh_reader.read_result(ali_file)

        if(len(results) == 0):
            sys.stderr.write("Warning: No alignment found in resultFile (%s)!\n" % (ali_file))
            fh.write("%s\t%s\t%s\t%s\n" % (alignment_pair[0], alignment_pair[1], "NA", "NA"))
            continue

        (query_ali, template_ali) = (results[0].query_ali, results[0].template_ali)

        (tp, tp_ref, tp_comp) = benchmark_alignments.compare_alignments(query_ref, template_ref, query_ali, template_ali)

        if(tp_ref == 0 or tp_comp == 0):
            sys.stderr.write("Warning: Invalid alignments (length of tm ali: %s) (length of ali: %s)!\n" % (tp_ref, tp_comp))
            fh.write("%s\t%s\t%s\t%s\n" % (alignment_pair[0], alignment_pair[1], "NA", "NA"))
            continue

        sensitivity = 1.0 * tp / tp_ref
        precision = 1.0 * tp / tp_comp

        fh.write("%s\t%s\t%s\t%s\n" % (alignment_pair[0], alignment_pair[1], str(sensitivity), str(precision)))
    fh.close()


def opt():
    parser = OptionParser()
    parser.add_option("--query_ffindex", dest="query_ffindex",
                        help="FFINDEX with query input", metavar="FFINDEX_PREFIX")
    parser.add_option("--template_ffindex", dest="template_ffindex",
                        help="FFINDEX with template input", metavar="FFINDEX_PREFIX")
    parser.add_option("--tm_dir", dest="tm_dir",
                        help="DIR with reference tm alignments", metavar="DIR")
    parser.add_option("--alignment_pairs", dest="alignment_pairs_file",
                        help="format of the input files", metavar="[hmmer|hhr|blast]")
    parser.add_option("--hhalign_parameters", dest="hhalign_parameters",
                        help="Additional parameters for hhalign", metavar="STRING")
    parser.add_option("-o", dest="output_file",
                        help="FILE for output of the statistics", metavar="FILE")
    parser.add_option("--hhalign_binary", dest="hhalign_binary",
                        help="path to hhalign binary", metavar="FILE")

    parser.set_default("hhalign_binary", "hhalign")
    parser.set_default("hhalign_parameters", "")

    return parser


def check_input(options, parser):
    if(not options.query_ffindex or not os.path.exists(options.query_ffindex+".ffindex") or not os.path.exists(options.query_ffindex+".ffdata")):
        sys.stderr.write("ERROR: query ffindex not specified or not found!\n")
        parser.print_help()
        sys.exit(1)

    if(not options.template_ffindex or not os.path.exists(options.template_ffindex+".ffindex") or not os.path.exists(options.template_ffindex+".ffdata")):
        sys.stderr.write("ERROR: query ffindex not specified or not found!\n")
        parser.print_help()
        sys.exit(1)

    if(not options.tm_dir or not os.path.exists(options.tm_dir)):
        sys.stderr.write("ERROR: directory with tm alignments not specified or not found!\n")
        parser.print_help()
        sys.exit(1)

    if(not options.output_file):
        sys.stderr.write("ERROR: output file not specified!\n")
        parser.print_help()
        sys.exit(1)


def main():
    parser = opt()
    (options, args) = parser.parse_args()
    check_input(options, parser)

    alignment_pairs = benchmark_alignments.read_alignment_pairs(options.alignment_pairs_file)

    benchmark_all_alignments(alignment_pairs, options.query_ffindex, options.template_ffindex,
                               options.hhalign_binary, options.hhalign_parameters,
                               options.tm_dir, options.output_file)

if __name__ == "__main__":
    main()
