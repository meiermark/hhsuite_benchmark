#!/usr/bin/awk -f



BEGIN {

    inConsensus = 0

}



/^>/ {

    inConsensus = 0

}



/^>.*_consensus/ {

    inConsensus = 1

}



inConsensus == 1 {

    print $0

    next

}
