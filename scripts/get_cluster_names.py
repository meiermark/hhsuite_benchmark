#!/usr/bin/env python

import ffindex
import sys

input_file = sys.argv[1]
output_file = sys.argv[2]

entries = ffindex.read_index(input_file+".ffindex")
data = ffindex.read_data(input_file+".ffdata")

with open(output_file, "w") as fh:
    for entry in entries:
        lines = ffindex.read_lines(entry, data)
        name = lines[0].split("|")[1]
        fh.write(entry.name+"\t"+name+"\n")
    
