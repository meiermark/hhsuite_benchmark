#!/usr/bin/env python

from optparse import OptionParser
from Bio.Blast import NCBIXML
import os
import sys
import tempfile
import re
import shutil
from collections import namedtuple

MIN_SEQ_IDENTITY = 0.3
MIN_COVERAGE = 0.9

Alignment = namedtuple("Alignment", "template_id, coverage, seq_identity")

def opt():
	parser = OptionParser();
	parser.add_option("-i",
		help="input fasta file", metavar="FILE",
		action="store", type="string", dest="input")
	parser.add_option("--a3m_ffdatabase",
		help="a3m ffindex database", metavar="PREFIX",
		action="store", type="string", dest="db")
	parser.add_option("--consensus_blast_database",
		help="blast database of a3m consensus sequences", metavar="PREFIX",
		action="store", type="string", dest="dbcons")
	parser.add_option("--blast",
		help="path to blast binary", metavar="BINARY", default="blastpgp",
		action="store", type="string", dest="blast_binary")
	parser.add_option("--hhalign",
		help="path to hhalign binary", metavar="BINARY", default="hhalign",
		action="store", type="string", dest="hhalign_binary")
	parser.add_option("--ffindex_get",
		help="path to ffindex_get binary", metavar="BINARY", default="ffindex_get",
		action="store", type="string", dest="ffindex_get_binary")
	parser.add_option("-d",
		help="output directory", metavar="DIR",
		action="store", type="string", dest="d")
	return parser

def runBlast(options):
	blast_output_file = tempfile.mkstemp(suffix=".xml", prefix="blast")[1]
	os.system(options.blast_binary+" -i "+options.input+" -d "+options.dbcons+" -a 1 -m 7 -o "+ blast_output_file)
	return blast_output_file

def parseBlast(blast_output_file, hits):
	blast_handle = open(blast_output_file)
	blast_records = NCBIXML.read(blast_handle)

	query_length = blast_records.query_length
	for alignment in blast_records.alignments:
		m = re.match("^[a-z]*\|([A-Z]+)\|", alignment.title.partition(" ")[2])
		if(m):
			template_id = m.group(1)
		else:
			template_id = alignment.title.partition(" ")[2]
		
		identities = 0
		aligned = 0
		end_pos_query = -1
		end_pos_template = -1

		for hsp in alignment.hsps:
			if(hsp.query_start > end_pos_query and hsp.sbjct_start > end_pos_template):
				identities += hsp.identities

				if(type(hsp.gaps) is int):
					aligned += len(hsp.query) - hsp.gaps
				else:
					aligned += len(hsp.query)

				end_pos_query = hsp.query_start + len(hsp.query) - hsp.query.count("-") - 1
				end_pos_template = hsp.sbjct_start + len(hsp.sbjct) - hsp.sbjct.count("-") - 1
				
		identity = 1.0 * identities / query_length
		coverage = 1.0 * aligned / query_length
		
		hit = Alignment(template_id, coverage, identity)
		hits.append(hit)

#hhalign has problems with input sequences consisting only of lower case characters
#therefore preprocess the inputfile sequence to upper case
def prepareFastaFile(options):
	outputfile = tempfile.mkstemp(suffix=".a3m", prefix="hit")[1]
	f = open(options.input)
	out = open(outputfile, 'w')

	for line in f:
		if(re.match(">", line)):
			out.write(line)
		else:
			out.write(line.upper())
	
	f.closed
	out.closed
	return outputfile


def calculateA3M(options, hits):
	outputfile = os.path.join(options.d, os.path.basename(options.input).partition(".seq")[0]+".a3m")
	inputfile = prepareFastaFile(options)
	#sort hits by identity (descending)
	hits = sorted(hits, key=lambda hit: hit.seq_identity, reverse=True)

	for hit in hits:
		if(hit.seq_identity >= MIN_SEQ_IDENTITY or hit.coverage >= MIN_COVERAGE):
			template_file = tempfile.mkstemp(suffix=".a3m", prefix="hit")[1]

			#get a3m of template form ffindex database and save to template_file
			short_id = hit.template_id.split("|")[1]
			ret = os.system(options.ffindex_get_binary+" "+options.db+".ffdata "+options.db+".ffindex "+ short_id + " > "+template_file)
			if(ret != 0):
				print("Error: template ("+short_id+") not found in ffindex database ("+options.db+")!")
				continue

			#call hhalign to create the a3m for the inputfile
			os.system(options.hhalign_binary+" -i "+inputfile+" -t "+template_file+" -oa3m "+ outputfile +" -mact 0.01 -seq 1000")

			os.remove(template_file)
			os.remove(inputfile)
			return
	
	print("Warning: No matching cluster consensus sequence found!")
	print("Warning: Use scop sequence ("+options.input+") as singleton!")

	shutil.move(inputfile, outputfile)
	

def main():
	parser = opt()
	(options, args) = parser.parse_args()

	#check input	
	if(options.input == None or options.db == None or options.dbcons == None or options.d == None):
		print("Parameter is missing!")
		parser.print_help()
		sys.exit(1)

	blast_output_file = runBlast(options)

	hits = []
	parseBlast(blast_output_file, hits)

	calculateA3M(options, hits)

	os.remove(blast_output_file)


if __name__ == '__main__':
	main()
