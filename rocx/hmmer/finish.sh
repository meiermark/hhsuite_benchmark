#!/bin/bash

WORK_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

dir=$1

cd ${dir}
database_bn=$(basename ${dir})

perl ${WORK_DIR}/../../scripts/hmmer2scores.pl ./ ../scop20_1.73/scop20_1.73_bench.fas 5287

ffindex_build -as ${database_bn}_hmmer_r1_scores.ff{data,index} *_1.scores
ffindex_build -as ${database_bn}_hmmer_r2_scores.ff{data,index} *_2.scores
ffindex_build -as ${database_bn}_hmmer_r3_scores.ff{data,index} *_3.scores

rm -rf *.hmm *.hmmer *.scores

cd ..
${WORK_DIR}/../eval.sh ${database_bn} ${database_bn}_hmmer
