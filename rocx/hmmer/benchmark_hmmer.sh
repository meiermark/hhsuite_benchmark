#!/bin/zsh

hmmer_bin_dir=${HOME}/opt/hmmer-3.1b2/bin
jackhmmer_bin=$hmmer_bin_dir/jackhmmer
hmmsearch_bin=$hmmer_bin_dir/hmmsearch
hmmbuild_bin=$hmmer_bin_dir/hmmbuild

input_dir=../scop20_1.73_bench

#usually the concatenated uniprot fasta database with the scop fasta sequences
database=/cbscratch/mmeier/data/hh-suite/benchmark_roc/psiblast/uniprot_2015_06_merged/uniprot20_2015_06_merged_scop.fas

db_name=$(basename ${database} .fas)
output_dir=/cbscratch/mmeier/data/hh-suite/benchmark_roc/hmmer/${db_name}

mkdir -p ${output_dir}
eval_threshold=100000

SEED=$1

bn=$(basename $SEED .seq)

${hmmbuild_bin} ${output_dir}/${bn}-0.hmm ${input_dir}/${SEED}
${hmmsearch_bin} -E ${eval_threshold} --tblout ${output_dir}/${bn}_1.hmmer --cpu 1 -o /dev/null ${output_dir}/${bn}-0.hmm ${database}

${jackhmmer_bin} --chkhmm ${output_dir}/${bn} -N 2 -o /dev/null --cpu 1 ${input_dir}/${SEED} ${database}

if [ -e ${output_dir}/${bn}-1.hmm ];
then
  ${hmmsearch_bin} -E ${eval_threshold} --tblout ${output_dir}/${bn}_2.hmmer --cpu 1 -o /dev/null ${output_dir}/${bn}-1.hmm ${database}
else
  echo "WARNING: no checkout file ${checkoutfile}"
  cp ${output_dir}/${bn}_1.hmmer ${output_dir}/${bn}_2.hmmer
fi

if [ -e ${output_dir}/${bn}-2.hmm ];
then
  ${hmmsearch_bin} -E ${eval_threshold} --tblout ${output_dir}/${bn}_3.hmmer --cpu 1 -o /dev/null ${output_dir}/${bn}-2.hmm ${database}
else
  echo "WARNING: no checkout file ${output_dir}/${bn}-2.hmm"
  cp ${output_dir}/${bn}_2.hmmer ${output_dir}/${bn}_3.hmmer
fi

