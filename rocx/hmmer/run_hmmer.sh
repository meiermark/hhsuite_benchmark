#!/bin/bash

while read f; 
do 
  bsub -m hh -n 1 -q mpi -R cbscratch -o ${HOME}/hmmer.log -a openmp -W 47:50 -J hmmer ./benchmark_hmmer.sh $f; 
done < ../scop20_1.73_bench/db.index
