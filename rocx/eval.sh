#!/usr/bin/zsh

dir=$1
prefix=$2

../scripts/benchmark_rocx.py --rocx=1 --score_ffindex="${dir}/${prefix}_r1_scores" -o ${dir}/${prefix}_roc1_r1 --annotation=./scop20_1.73_bench/identifier_to_family.dat &
../scripts/benchmark_rocx.py --rocx=1 --score_ffindex="${dir}/${prefix}_r2_scores" -o ${dir}/${prefix}_roc1_r2 --annotation=./scop20_1.73_bench/identifier_to_family.dat &
../scripts/benchmark_rocx.py --rocx=1 --score_ffindex="${dir}/${prefix}_r3_scores" -o ${dir}/${prefix}_roc1_r3 --annotation=./scop20_1.73_bench/identifier_to_family.dat &

