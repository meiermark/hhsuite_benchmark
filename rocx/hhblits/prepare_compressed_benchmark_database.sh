#!/bin/zsh

# Cluster specific instructions

#BSUB -q mpi
#BSUB -W 47:50
#BSUB -n 16
#BSUB -a openmp
#BSUB -o /usr/users/mmeier/jobs/prepare_benchmark_database.log
#BSUB -R "span[hosts=1]"
#BSUB -R np16
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -J prepare_benchmark_database
#BSUB -m hh

source ${HOME}/.zshrc

source /etc/profile
source $HOME/.zshrc

module use-append $HOME/modulefiles/
module load gcc/4.9.1
module load intel/compiler/64/14.0/2013_sp1.3.174
module load python/3.5.0

# Setting paths / variables

#assumed to run on a single node at the moment
NCORES=16

SCRIPTS_DIR=./../../scripts
SCOP_DIR=./../scop20_1.73_bench
BLAST_BIN_DIR=/usr/users/mmeier/opt/blast-2.2.26/bin
#a3m_database_extract ffindex_build ffindex_apply_mpi ffindex_apply ffindex_get hhalign cstranslate hhmake are expected to be in the PATH
#HHLIB environment variable needs to be set properly (see hh-suite userguide)
#python3 with biopython needs to be installed

#path to compressed hh-suite database
input=/cbscratch/mmirdit/databases/uniboost_2016_03/uniboost10_2016_03
input_db_name=$(basename ${input})

output_dir=/cbscratch/mmeier/data/hh-suite/benchmark_roc/databases/${input_db_name}
mkdir -p ${output_dir}

echo "Preparing benchmark database for hh-suite database:" ${input_db_name}

cp ${input}_cs219.ff* ${output_dir}

a3m_database_extract -i ${input}_ca3m -o ${output_dir}/${input_db_name}_a3m -d ${input}_sequence -q ${input}_header
ffindex_build -as ${output_dir}/${input_db_name}_a3m.ff{data,index}

python3 ${SCRIPTS_DIR}/get_cluster_names.py ${output_dir}/${input_db_name}_a3m ${output_dir}/name_mapping.dat
#TODO duplicate names will be skipped
python3 ${SCRIPTS_DIR}/rename_clusters.py ${output_dir}/${input_db_name}_a3m.ffindex ${output_dir}/name_mapping.dat ${output_dir}/${input_db_name}_a3m.ffindex
ffindex_build -as ${output_dir}/${input_db_name}_a3m.ff{data,index}

#TODO duplicate names will be skipped
python3 ${SCRIPTS_DIR}/rename_clusters.py ${output_dir}/${input_db_name}_cs219.ffindex ${output_dir}/name_mapping.dat ${output_dir}/${input_db_name}_cs219.ffindex
ffindex_build -as ${output_dir}/${input_db_name}_cs219.ff{data,index}

mpirun -np ${NCORES} ffindex_apply_mpi ${output_dir}/${input_db_name}_a3m.ff{data,index} -i ${output_dir}/${input_db_name}_a3m_consensus.ffindex -d ${output_dir}/${input_db_name}_a3m_consensus.ffdata -- hhconsensus -i stdin -o stdout -v 0

rm -f ${output_dir}/${input_db_name}_a3m.ff{data,index}
mv -f ${output_dir}/${input_db_name}_a3m_consensus.ffindex ${output_dir}/${input_db_name}_a3m.ffindex
mv -f ${output_dir}/${input_db_name}_a3m_consensus.ffdata ${output_dir}/${input_db_name}_a3m.ffdata

mpirun -np ${NCORES} ffindex_apply_mpi ${output_dir}/${input_db_name}_a3m.ff{data,index} -i ${output_dir}/consensus.ffindex -d ${output_dir}/consensus.ffdata -- ${SCRIPTS_DIR}/get_consensus.awk
sed "s/\x0//g" ${output_dir}/consensus.ffdata > ${output_dir}/consensus.fa
cd ${output_dir}
${BLAST_BIN_DIR}/formatdb -i consensus.fa -p T

echo "map scop to a3m clusters"
mkdir -p ${output_dir}/scop
cat ${SCOP_DIR}/db.index | parallel -j ${NCORES} python3 ${SCRIPTS_DIR}/map_validation_cluster.py -i ${SCOP_DIR}/{} --db=${output_dir}/${input_db_name}_a3m --dbcons=consensus.fa -d ${output_dir}/scop_a3m --blast=${BLAST_BIN_DIR}/blastpgp

cd ${output_dir}/scop
ffindex_build -as ${output_dir}/scop/scop_a3m.ff{data,index} *.a3m

echo "calculate column state sequences for scop"
#WARNING: OMP_NUM_THREADS should be set at maximum to the number of cores on the node...
export OMP_NUM_THREADS=${NCORES}
cstranslate -A ${HHLIB}/data/cs219.lib -D ${HHLIB}/data/context_data.lib -x 0.3 -c 4 -i ${output_dir}/scop/scop_a3m -o ${output_dir}/scop/scop_cs219 -I a3m -b -f
ffindex_build -as ${output_dir}/scop/scop_cs219.ff{data,index}

ffindex_build -as ${output_dir}/${input_db_name}_cs219.ff{data,index} -i ${output_dir}/scop/scop_cs219.ffindex -d ${output_dir}/scop/scop_cs219.ffdata
ffindex_build -as ${output_dir}/${input_db_name}_a3m.ff{data,index} -i ${output_dir}/scop/scop_a3m.ffindex -d ${output_dir}/scop/scop_a3m.ffdata

#calculate hhm for clusters with more than 50 sequences
echo "select huge a3ms for hhm generation"
mpirun -np ${NCORES} ffindex_apply_mpi ${output_dir}/${input_db_name}_a3m.ff{data,index} -i ${output_dir}/${input_db_name}_sizes.ffindex -d ${output_dir}/${input_db_name}_sizes.ffdata -- grep -c "^>"
python3 ${SCRIPTS_DIR}/get_small_a3m_entries.py ${output_dir}/${input_db_name}_sizes ${output_dir}/small_a3ms.dat
cp ${output_dir}/${input_db_name}_a3m.ffindex ${output_dir}/big_a3m.ffindex
ffindex_modify -u -f ${output_dir}/small_a3ms.dat ${output_dir}/big_a3m.ffindex

echo "calculate hhm"
mpirun -np ${NCORES} ffindex_apply_mpi ${output_dir}/${input_db_name}_a3m.ffdata ${output_dir}/big_a3m.ffindex -i ${output_dir}/${input_db_name}_hhm.ffindex -d ${output_dir}/${input_db_name}_hhm.ffdata -- hhmake -i stdin -o stdout -v 0


