#!/bin/zsh

#BSUB -q mpi
#BSUB -W 47:50
#BSUB -n 16
#BSUB -a openmp
#BSUB -o /usr/users/mmeier/jobs/roc5.log
#BSUB -R "span[hosts=1]"
#BSUB -R np16
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -J roc5
#BSUB -m hh

source ${HOME}/.zshrc
source /etc/profile
source $HOME/.zshrc

module use-append $HOME/modulefiles/
module load gcc/4.9.1
module load intel/compiler/64/14.0/2013_sp1.3.174

NCORES=16
HHBLITS_BIN=hhblits

mkdir -p /local/${USER}
MYLOCAL=$(mktemp -d --tmpdir=/local/${USER})

#copy input ffdatabase to local node ssd
src_input=../scop20_1.73_bench/scop20_1.73_bench
input_basename=$(basename ${src_input})
cp ${src_input}.ff* ${MYLOCAL}
input=${MYLOCAL}/${input_basename}

#copy benchmark database to local node ssd
src_database=/cbscratch/mmeier/databases/uniprot20_2015_06_merged
database_basename=$(basename ${src_database})
cp -r ${src_database} ${MYLOCAL}
database=${MYLOCAL}/${database_basename}/${database_basename}

output_dir=/cbscratch/mmeier/data/hh-suite/benchmark_roc/${database_basename}
mkdir -p ${output_dir}

mpirun -np ${NCORES} ffindex_apply_mpi ${input}.ff{data,index} -d ${MYLOCAL}/${database_basename}_r1_scores.ffdata -i ${MYLOCAL}/${database_basename}_r1_scores.ffindex -- ${HHBLITS_BIN} -i stdin -d ${database} -scores stdout -min_prefilter_hits 100 -n 1 -cpu 1 -ssm 0 -v 0 -wg -neffmax 20 -interim_id_filter
cp ${MYLOCAL}/${database_basename}_r1_scores* ${output_dir}

mpirun -np ${NCORES} ffindex_apply_mpi ${input}.ff{data,index} -d ${MYLOCAL}/${database_basename}_r2_scores.ffdata -i ${MYLOCAL}/${database_basename}_r2_scores.ffindex -- ${HHBLITS_BIN} -i stdin -d ${database} -scores stdout -min_prefilter_hits 100 -n 2 -cpu 1 -ssm 0 -v 0 -wg -neffmax 20 -interim_id_filter
cp ${MYLOCAL}/${database_basename}_r2_scores* ${output_dir}

mpirun -np ${NCORES} ffindex_apply_mpi ${input}.ff{data,index} -d ${MYLOCAL}/${database_basename}_r3_scores.ffdata -i ${MYLOCAL}/${database_basename}_r3_scores.ffindex -- ${HHBLITS_BIN} -i stdin -d ${database} -scores stdout -min_prefilter_hits 100 -n 3 -cpu 1 -ssm 0 -v 0 -wg -neffmax 20 -interim_id_filter
cp ${MYLOCAL}/${database_basename}_r3_scores* ${output_dir}

rm -rf ${MYLOCAL}
