#!/bin/bash

WORK_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

dir=$1

cd ${dir}
database_bn=$(basename ${dir})

${WORK_DIR}/../../scripts/blast2scores.py "*.blast"

ffindex_build -as ${database_bn}_psiblast_r1_scores.ff{data,index} *_1.scores
ffindex_build -as ${database_bn}_psiblast_r2_scores.ff{data,index} *_2.scores
ffindex_build -as ${database_bn}_psiblast_r3_scores.ff{data,index} *_3.scores

rm -rf *.blast *.scores

cd ..
${WORK_DIR}/../eval.sh ${database_bn} ${database_bn}_psiblast
