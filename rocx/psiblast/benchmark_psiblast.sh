#!/bin/zsh

psiblast_bin=/usr/users/mmeier/opt/blast-2.2.31+/bin/psiblast

input_dir=../scop20_1.73_bench

#usually the concatenated uniprot fasta database with the scop fasta sequences; processed with blast's makeblastdb
database=/cbscratch/mmeier/data/hh-suite/benchmark_roc/psiblast/uniprot_2015_06_merged/uniprot20_2015_06_merged_scop.fas
db_name=$(basename $database)

output_dir=/cbscratch/mmeier/data/hh-suite/benchmark_roc/psiblast/${db_name}

mkdir -p ${output_dir}

SEED=$1

bn=$(basename $SEED .seq)

${psiblast_bin} -query ${input_dir}/${SEED} -db ${database} -out ${output_dir}/${bn}_1.blast -evalue 10000 -num_descriptions 250000 -num_alignments 1 -num_threads 1 -num_iterations 1
${psiblast_bin} -query ${input_dir}/${SEED} -db ${database} -out ${output_dir}/${bn}_2.blast -evalue 10000 -num_descriptions 250000 -num_alignments 1 -num_threads 1 -num_iterations 2
${psiblast_bin} -query ${input_dir}/${SEED} -db ${database} -out ${output_dir}/${bn}_3.blast -evalue 10000 -num_descriptions 250000 -num_alignments 1 -num_threads 1 -num_iterations 3


