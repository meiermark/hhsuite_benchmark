#!/bin/zsh

#$ -t 1-35132
#$ -q twin-E5-2680
#$ -o /cluster/user/meiermark/jobs/build_hmmer_alis.log
#$ -pe threadssl6.pe 1

source /etc/profile
source /net/cluster/user/meiermark/.zshrc

hmmer_bin_dir=/data/fs03/scratch_raptor_raid/meiermark/benchmark_hmmer/hmmer/binaries
hmmsearch_bin=${hmmer_bin_dir}/hmmsearch

SEEDFILE=/data/fs03/scratch_raptor_raid/meiermark/benchmark_alignment/combinations.dat
QUERY=$(sed -n "${SGE_TASK_ID}p" ${SEEDFILE} | awk '{print $1}')
TEMPLATE=$(sed -n "${SGE_TASK_ID}p" ${SEEDFILE} | awk '{print $2}')

INPUT_DIR=/data/fs03/scratch_raptor_raid/meiermark/benchmark_alignment/hmmer_uniprot_2012_10
OUTPUT_DIR=$INPUT_DIR


for QUERY_ITERATION in 2it;
do
	${hmmsearch_bin} ${INPUT_DIR}/${QUERY_ITERATION}/${QUERY}.hmm ${INPUT_DIR}/0it/${TEMPLATE}.fa > ${OUTPUT_DIR}/${QUERY_ITERATION}/${QUERY}-${TEMPLATE}.hmmer
done
