#!/bin/zsh

#run prepare_input_hhalign.sh before; adjust script to your needs
#see below; iterations for the query
it=1

for mact in 0.01 0.1 0.2 0.35 0.5;
do
  ../../scripts/build_benchmark_alignments.py --query_ffindex=${prefix}_${it}it_a3m --template_ffindex=${prefix}_templates_a3m --tm_dir=../tm_alis --alignment_pairs=../combinations_ext.dat --hhalign_parameters="-mact ${mact} -v 0 -wg" -o ${prefix}_mact_${mact}.dat &
done
