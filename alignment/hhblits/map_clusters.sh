#!/usr/bin/zsh +vx

GET_FIRST_HIT_SCRIPT=../../scripts/get_first_hit.py
GET_QUERY_SCRIPT=../../scripts/get_query.py
HHALIGN_BINARY=hhalign

bench_database=$1
database=$2

#prepare hhr
tmp_hhr=$(mktemp -p /dev/shm --suffix=".hhr")
cat > ${tmp_hhr}

#prepare template
first_hit=$(${GET_FIRST_HIT_SCRIPT} ${tmp_hhr})
tmp_hit=$(mktemp -p /dev/shm --suffix=".a3m")
ffindex_get ${database}_a3m.ff{data,index} ${first_hit}.a3m > ${tmp_hit}

#prepare query
query=$(${GET_QUERY_SCRIPT} ${tmp_hhr})
tmp_query=$(mktemp -p /dev/shm --suffix=".a3m")
ffindex_get ${bench_database}.ff{data,index} ${query}.a3m > ${tmp_query}

#make alignment
${HHALIGN_BINARY} -i ${tmp_query} -t ${tmp_hit} -oa3m stdout -v 0

rm ${tmp_hhr} ${tmp_hit} ${tmp_query}
