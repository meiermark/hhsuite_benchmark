#!/usr/bin/zsh

source ~/.zshrc

HHBLITS_OMP_BINARY=hhblits_omp

MAP_CLUSTERS_SCRIPT=./map_clusters.sh
#for compressed databases:
#MAP_CLUSTERS_SCRIPT=./map_compressed_clusters.sh

BENCH_DATABASE=../bench_fasta
DATABASE=uniprot20_2012_10_1it_70

prefix=$(basename ${DATABASE})
OUTPUT_DIR=./${prefix}
mkdir ${OUTPUT_DIR}
output_database=${OUTPUT_DIR}/${prefix}

${HHBLITS_OMP_BINARY} -i ${BENCH_DATABASE} -d ${DATABASE} -oa3m ${output_database}_1it_a3m -o ${output_database}_1it_hhr -n 1 -cpu 16 -v 1

#for the query you can either use the fasta sequence (../bench_fasta), the 1it a3m's (${output_database}_1it_a3m) or further itarations
#it=3
#${HHBLITS_OMP_BINARY} -i ${BENCH_DATABASE} -d ${DATABASE} -oa3m ${output_database}_${it}it_a3m -n ${it} -cpu 16 -v 1

# for the template we need the mapping of the fasta sequences to the best clusters in the database...
mpirun -np 12 ffindex_apply_mpi ${output_database}_1it_hhr.ff{data,index} -d ${output_database}_templates_a3m.ffdata -i ${output_database}_templates_a3m.ffindex -- ${MAP_CLUSTERS_SCRIPT} ${BENCH_DATABASE} ${DATABASE}
